package vn.toancauxanh.gg.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.Window;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.model.Model;

@Entity
@Table(name = "khoa", indexes = { @Index(columnList = "name")})
public class Khoa extends Model<Khoa>{
	public static transient final Logger LOG = LogManager.getLogger(Khoa.class.getName());
	public Khoa() {
		
	}
	
	private String name;
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Command
	public void saveKhoaMain(@BindingParam("list") final Object listObject,
			@BindingParam("wdn") final Window wdn) {
		setName(getName().trim().replaceAll("\\s+", " "));
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, "*");
	}
	
	@Transient
	public AbstractValidator getValidatorKhoa() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				String value = (String)ctx.getProperty().getValue();
				if(value == null || "".equals(value)) {
					addInvalidMessage(ctx, "error","Không được để trống trường này");
				} /*else {
					JPAQuery<Category> q = find(Category.class)
							.where(QCategory.category.name.eq(value))
							.where(QCategory.category.trangThai.ne(core().TT_DA_XOA));
					if(getParent() == null) {
						q.where(QCategory.category.parent.isNull());
					} else {
						q.where(QCategory.category.parent.eq(getParent()));
					}
					if(!Category.this.noId()) {
						q.where(QCategory.category.id.ne(getId()));
					}
					if(q.fetchCount() > 0) {
						addInvalidMessage(ctx, "error","Tên chủ đề đã được sử dụng");
					}
				}*/
			}
		};
	}
}
