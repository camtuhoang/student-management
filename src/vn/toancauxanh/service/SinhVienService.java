package vn.toancauxanh.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.model.SinhVien;
import vn.toancauxanh.model.QSinhVien;
import vn.toancauxanh.model.QVaiTro;
import vn.toancauxanh.model.VaiTro;

public final class SinhVienService extends BasicService<SinhVien> {

	public SinhVien getSinhVien(boolean saving) {
		if (Executions.getCurrent() == null) {
			return null;
		}
		return getSinhVien(saving, (HttpServletRequest) Executions.getCurrent().getNativeRequest(),
				(HttpServletResponse) Executions.getCurrent().getNativeResponse());
	}

	public JPAQuery<SinhVien> getTargetQuerySinhVien() {
		String paramTrangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "").trim();
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"), "").trim();
		Long paramVaiTro = (Long) argDeco().get(Labels.getLabel("param.vaitro"));
//		Long paramDonVi = (Long) argDeco().get(Labels.getLabel("param.donvi"));

		JPAQuery<SinhVien> q = find(SinhVien.class).where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			q.where(QSinhVien.sinhVien.hoVaTen.containsIgnoreCase(tuKhoa)
					.or(QSinhVien.sinhVien.maSinhVien.containsIgnoreCase(tuKhoa)));
		}

		if (paramVaiTro != null) {
			VaiTro vaiTro = find(VaiTro.class).where(QVaiTro.vaiTro.id.eq(paramVaiTro)).fetchFirst();
			q.where(QSinhVien.sinhVien.vaiTros.contains(vaiTro));
		}
		/*if (paramDonVi != null) {
			q.where(QSinhVien.sinhVien.donVi.id.eq(paramDonVi).or(QSinhVien.nhanVien.donVi.cha.id.eq(paramDonVi)));
		}*/

		if (paramTrangThai != null && !paramTrangThai.isEmpty()) {
			q.where(QSinhVien.sinhVien.trangThai.eq(paramTrangThai));
		}
		q.orderBy(QSinhVien.sinhVien.trangThai.asc());
		return q.orderBy(QSinhVien.sinhVien.ngaySua.desc());
	}

	@Command
	public void login(@BindingParam("email") final String email, @BindingParam("password") final String password) {
		// System.out.println("email: " + email);
		// System.out.println("password: " + password);
		SinhVien sinhVien = new JPAQuery<SinhVien>(em()).from(QSinhVien.sinhVien)
				.where(QSinhVien.sinhVien.daXoa.isFalse()).where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA))
				.where(QSinhVien.sinhVien.maSinhVien.eq(email))
				.fetchFirst();
		BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
		if (sinhVien != null
				&& encryptor.checkPassword(password.trim() + sinhVien.getSalkey(), sinhVien.getMatKhau())) {
			// System.out.println("nhan vien != null");
			String cookieToken = sinhVien
					.getCookieToken(System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(6, TimeUnit.HOURS));
			Session zkSession = Sessions.getCurrent();
			zkSession.setAttribute("email", cookieToken);
			HttpServletResponse res = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
			Cookie cookie = new Cookie("email", cookieToken);
			cookie.setPath("/");
			cookie.setMaxAge(1000000000);
			res.addCookie(cookie);
			Executions.sendRedirect("/cp");
		} else {
			showNotification("Đăng nhập không thành công", "", "error");
		}
	}

	@Command
	public void logout() {
		// System.out.println("logout");
		SinhVien SinhVienLogin = getSinhVien(true);
		if (SinhVienLogin != null && !SinhVienLogin.noId()) {
			Session zkSession = Sessions.getCurrent();
			zkSession.removeAttribute("email");
			HttpServletResponse res = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
			Cookie cookie = new Cookie("email", null);
			cookie.setPath("/");
			cookie.setMaxAge(0);
			res.addCookie(cookie);
			Executions.sendRedirect("/login");
		}
	}

	public List<SinhVien> getTacGias() {
		// TODO add them dk nhan vien là tác giả
		return getTargetQuerySinhVien().fetch();
	}

	public List<SinhVien> getTacGiasAndNull() {
		// TODO add them dk nhan vien là tác giả
		List<SinhVien> list = new ArrayList<>();
		list.add(null);
		list.addAll(getTargetQuerySinhVien().fetch());
		return list;
	}

	private List<SinhVien> tacGiasTimKiem = new ArrayList<>();

	public List<SinhVien> getTacGiasTimKiem() {
		if (tacGiasTimKiem.size() == 0) {
			tacGiasTimKiem = getTacGiasAndNull();
		}
		return tacGiasTimKiem;
	}

	@Command
	public void timKiems(@BindingParam("hoTenTacGia") @Default(value = "") final String name,
			@BindingParam("baiViet") final Object bv) {

		if (name.isEmpty()) {
			tacGiasTimKiem = getTacGiasAndNull();
		} else {
			tacGiasTimKiem.clear();
			tacGiasTimKiem.addAll(find(SinhVien.class).where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA))
					.where(QSinhVien.sinhVien.hoVaTen.like("%" + name + "%")).orderBy(QSinhVien.sinhVien.hoVaTen.asc())
					.fetch());
		}
		BindUtils.postNotifyChange(null, null, this, "tacGiasTimKiem");
	}
}
