package vn.toancauxanh.cms.service;

import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.Category;
import vn.toancauxanh.gg.model.Khoa;
import vn.toancauxanh.gg.model.LopHoc;
import vn.toancauxanh.gg.model.QCategory;
import vn.toancauxanh.gg.model.QKhoa;
import vn.toancauxanh.gg.model.QLopHoc;
import vn.toancauxanh.service.BasicService;

public class LopHocService extends BasicService<LopHoc>{

	public JPAQuery<LopHoc> getTargetQuery() {
		String paramImage = MapUtils.getString(argDeco(),Labels.getLabel("param.tukhoa"), "").trim();
		String trangThai = MapUtils.getString(argDeco(),Labels.getLabel("param.trangthai"),"");
		JPAQuery<LopHoc> q = find(LopHoc.class)
				.where(QLopHoc.lopHoc.trangThai.ne(core().TT_DA_XOA));
		
		if (paramImage != null && !paramImage.isEmpty()) {
			String tukhoa = "%" + paramImage + "%";
			q.where(QLopHoc.lopHoc.description.like(tukhoa));
		}
		if (!trangThai.isEmpty()) {
			q.where(QLopHoc.lopHoc.trangThai.eq(trangThai));
		}
		q.orderBy(QLopHoc.lopHoc.ngaySua.desc());
		return q;
	}
	
	public List<LopHoc> getListAllLopHoc() {
		JPAQuery<LopHoc> q = find(LopHoc.class);
		q.where(QLopHoc.lopHoc.trangThai.ne(core().TT_DA_XOA));
				//.where(QLopHoc.lopHoc.parent.isNull());
		//q.orderBy(QLopHoc.lopHoc.soThuTu.asc());
		List<LopHoc> list = q.fetch();
		/*for (LopHoc category : list) {
			category.loadChildren();
		}*/
		return list;
	}
	
	/*private Khoa selectedKhoa;
	private LopHoc selectedLopHoc;
	
	@Override
	public Khoa getSelectedKhoa() {
		return selectedKhoa;
	}
	
	@Override
	public void setSelectedKhoa(Khoa selectedKhoa) {
		this.selectedKhoa = selectedKhoa;
		selectedLopHoc = null;
		BindUtils.postNotifyChange(null, null, this, "selectedLopHoc");
	}*/
}
