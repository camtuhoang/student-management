package vn.toancauxanh.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.google.common.base.Strings;
import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.Asset;
import vn.toancauxanh.gg.model.FileEntry;
import vn.toancauxanh.gg.model.Khoa;
import vn.toancauxanh.gg.model.LopHoc;
import vn.toancauxanh.service.Quyen;
import vn.toancauxanh.service.ResizeHinhAnh;

@Entity
@Table(name = "sinhvien", indexes = { @Index(columnList = "chucVu"), @Index(columnList = "diaChi"),
		@Index(columnList = "email"), @Index(columnList = "hinhDaiDien"), @Index(columnList = "hoVaTen"),
		@Index(columnList = "ngaySinh"), @Index(columnList = "soDienThoai"), @Index(columnList = "maSinhVien"),
		@Index(columnList = "checkKichHoat") })
// @SequenceGenerator(name = "per_class_gen", sequenceName =
// "HIBERNATE_SEQUENCE", allocationSize = 1)
public class SinhVien extends Asset<SinhVien> {
	
	public static transient final Logger LOG = LogManager.getLogger(SinhVien.class.getName());
	
/*	public static final String TONGBIENTAP = "tongbientap";
	public static final String CONGTACVIEN = "congtacvien";
	public static final String BIENTAPVIEN = "bientapvien";
	public static final String QUANTRIVIEN = "quantrivien";*/
	public static final String TONGBIENTAP = "tongbientap";
	private String chucVu = "";
	private String diaChi = "";
	private String email = "";
	private String hinhDaiDien = "";
	private String hoVaTen = "";
	private String matKhau = "";
	private String salkey = "";
	private String soDienThoai = "";
	private String maSinhVien = "";
	private Date ngaySinh;
	private Set<String> quyens = new HashSet<>();
	private Set<String> tatCaQuyens = new HashSet<>();
	private Set<VaiTro> vaiTros = new HashSet<>();
	private boolean selectedDV;
	private String matKhau2 = "";
	private Khoa khoa;
	private LopHoc lopHoc;
	private vn.toancauxanh.gg.model.Image avatarImage;
	private List<FileEntry> fileEntries = new ArrayList<>();
	
	private Quyen quyen = new Quyen(new SimpleAccountRealm() {
		@Override
		protected AuthorizationInfo getAuthorizationInfo(final PrincipalCollection arg0) {
			final SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			//info.setStringPermissions(getTatCaQuyens());
			return info;
		}
	});

	@Override
	public String toString() {
		return super.toString() + " " + maSinhVien + " " + getVaiTros();
	}
		
	public String getSalkey() {
		return salkey;
	}


	public void setSalkey(String salkey) {
		this.salkey = salkey;
	}
	
	@Transient
	public String getMatKhau2() {
		return matKhau2;
	}

	public void setMatKhau2(String matKhau2) {
		this.matKhau2 = matKhau2;
	}
	
	@ManyToOne
	public Khoa getKhoa() {
		return khoa;
	}

	public void setKhoa(Khoa khoa) {
		this.khoa = khoa;
	}
	@ManyToOne
	public LopHoc getLopHoc() {
		return lopHoc;
	}

	public void setLopHoc(LopHoc lopHoc) {
		this.lopHoc = lopHoc;
	}



	private Image imageContent;
	
	private boolean flagImage = true;
	@Transient
	public  Image getImageContent() {
		if (imageContent == null && !noId()
				&& !core().TT_DA_XOA.equals(getTrangThai())) {
			if (flagImage) {
				System.out.println("-----------1---------");
				try {
					loadImageIsView();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return imageContent;
	}
	
	private void loadImageIsView() throws FileNotFoundException, IOException {
		String imgName = "";
		String path = "";
		path = folderStore() + getHoVaTen();
		if (new File(path).exists()) {
			
			try (FileInputStream fis = new FileInputStream(new File(path));){
				setImageContent(new AImage(imgName, fis));
			}
		}
	}
	
	public void setImageContent( org.zkoss.image.Image _imageContent) {
		this.imageContent = _imageContent;
	}
	
	@Command
	public void attachImages(@BindingParam("media") final Media media) {
		LOG.info("attachImages articles");
		if (media instanceof org.zkoss.image.Image) {
			if(media.getName().toLowerCase().endsWith(".png")
				|| media.getName().toLowerCase().endsWith(".jpg")){
				int lengthOfImage = media.getByteData().length;
				if (lengthOfImage > 10000000) {
			        showNotification("Chọn hình ảnh có dung lượng nhỏ hơn 10MB.", "", "error");
			        return;
				}
				else {
					String tenFile = media.getName();
					tenFile = tenFile.replace(" ", "");
					tenFile = unAccent(tenFile.substring(0, tenFile.lastIndexOf("."))) + "_"
							+ Calendar.getInstance().getTimeInMillis()
							+ tenFile.substring(tenFile.lastIndexOf("."));
					
					vn.toancauxanh.gg.model.Image avatarImage2 = getAvatarImage();
					if (avatarImage2 == null) {
						avatarImage2 = new vn.toancauxanh.gg.model.Image();
					}
					setAvatarImage(avatarImage2);
					avatarImage2.setImageContent((org.zkoss.image.Image) media);
					avatarImage2.setName(tenFile);
					BindUtils.postNotifyChange(null, null, this, "avatarImage");
				}
			}
			else {
				showNotification("Chọn hình ảnh theo đúng định dạng (*.png, *.jpg)","","error");
			}
		} else {
			showNotification("File tải lên không phải hình ảnh!", "", "error");
		}
	}
	
	@Command
	public void deleteImg() {
		LOG.info("deleteImg" + getAvatarImage());
		setAvatarImage(null);
		BindUtils.postNotifyChange(null, null, this, "avatarImage");
	}

	protected void saveImage() throws IOException {
		vn.toancauxanh.gg.model.Image avatar = getAvatarImage();
		if (avatar != null) {
			org.zkoss.image.Image imageContent = avatar.getImageContent();
			if (imageContent != null) {
				// luu hinh
				LOG.info("saveImage() :" + folderStore() + File.separator
						+ imageContent.getName());
				avatar.setImageUrl(folderImageUrl().concat(avatar.getName()));
				LOG.info("ImageUrl: " + folderImageUrl().concat(avatar.getName()));
				avatar.setMedium(folderImageUrl().concat("m_" + avatar.getName()));
				LOG.info("Image Medium: " + folderImageUrl().concat("m_" + avatar.getName()));
				avatar.setSmall(folderImageUrl().concat("s_" + avatar.getName()));
				final File baseDir = new File(avatar.folderStore().concat(avatar.getName()));
				Files.copy(baseDir, imageContent.getStreamData());
				/*ResizeHinhAnh.saveMediumAndSmall(avatar, avatar.folderStore());*/ 
				ResizeHinhAnh.saveMediumAndSmall2(avatar,
						avatar.folderStore());
			}
		}
	}
	
	public String folderImageUrl() {
		return "/" + Labels.getLabel("filestore.folder") + "/image/";
	}
	

	@ElementCollection(fetch = FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	//@Fetch(FetchMode.SUBSELECT)
	@CollectionTable(name = "sinhvien_quyens", joinColumns = {@JoinColumn(name = "sinhVien_id")})
	public Set<String> getQuyens() {
		return quyens;
	}

	@Transient
	public Set<String> getTatCaQuyens() {
		if (tatCaQuyens.isEmpty()) {
			tatCaQuyens.addAll(quyens);
			for (VaiTro vaiTro : vaiTros) {
				if (!vaiTro.getAlias().isEmpty()) {
					//tatCaQuyens.add(vaiTro.getAlias());
				}
				tatCaQuyens.addAll(vaiTro.getQuyens());
			}
			if (Labels.getLabel("email.superuser").equals(maSinhVien)) {
				tatCaQuyens.add("*");
			}
		}
		return tatCaQuyens;
	}

	public void setQuyens(final Set<String> dsChoPhep) {
		quyens = dsChoPhep;
	}


	@Transient
	public String getVaiTroText() {
		String result = "";
		for (VaiTro vt : getVaiTros()) {
			result += (result.isEmpty() ? "" : ", ") + vt.getTenVaiTro();
		}
		return result;
	}
	
	@Transient
	public String getFirstAlias() {
		String result = "";
		for (VaiTro vt : getVaiTros()) {
			result = vt.getAlias();
			break;
		}
		return result;
	}

	public SinhVien() {
		super();
	}

	public SinhVien(final String maSinhVien_, final String _hoTen) {
		super();
		maSinhVien = maSinhVien_;
		hoVaTen = _hoTen;
	}

	@Override
	public void doSave() {
		super.doSave();
	}

	public String getChucVu() {
		return chucVu;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public String getEmail() {
		return email;
	}

	public String getHinhDaiDien() {
		return hinhDaiDien;
	}

	public String getHoVaTen() {
		return hoVaTen;
	}
	

	public String getMatKhau() {
		return matKhau;
	}

	public Date getNgaySinh() {
		return ngaySinh;
	}

	public String getSoDienThoai() {
		return soDienThoai;
	}

	public String getMaSinhVien() {
		return maSinhVien;
	}
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "sinhvien_vaitro", joinColumns = {
			@JoinColumn(name = "sinhvien_id") }, inverseJoinColumns = { @JoinColumn(name = "vaitros_id") })
	//@Fetch(value = FetchMode.SUBSELECT)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	public Set<VaiTro> getVaiTros() {
		return vaiTros;
	}

	// @Transient
	// public boolean isSuperUser() {
	// return realm.isPermitted(null, "*");
	// }

	
	public void setChucVu(final String _chuVu) {
		chucVu = Strings.nullToEmpty(_chuVu);
	}

	public void setDiaChi(final String _diaChi) {
		diaChi = Strings.nullToEmpty(_diaChi);
	}

	public void setEmail(final String _email) {
		email = Strings.nullToEmpty(_email);
	}
	
	/*public void setMaSinhVien(String maSinhVien) {
		this.maSinhVien = maSinhVien;
	}*/

	public void setHinhDaiDien(final String _hinhDaiDien) {
		hinhDaiDien = Strings.nullToEmpty(_hinhDaiDien);
	}

	public void setHoVaTen(final String _hoVaTen) {
		hoVaTen = Strings.nullToEmpty(_hoVaTen);
	}

	public void setMatKhau(final String _matKhau) {
		matKhau = Strings.nullToEmpty(_matKhau);
	}

	public void setNgaySinh(final Date _ngaySinh) {
		ngaySinh = _ngaySinh;
	}

	public void setSoDienThoai(final String _soDienThoai) {
		soDienThoai = Strings.nullToEmpty(_soDienThoai);
	}

	public void setMaSinhVien(final String _maSinhVien) {
		maSinhVien = Strings.nullToEmpty(_maSinhVien);
	}

	public void setVaiTros(final Set<VaiTro> vaiTros1) {
		vaiTros = vaiTros1;
	}

	@Transient
	public Quyen getTatCaQuyen() {
		return quyen;
	}
	@Transient
	public boolean isTongBienTap() {
		return core().getQuyen().get(TONGBIENTAP); // entry.quyen.tongbientap
	}
	
	@ManyToOne
	public vn.toancauxanh.gg.model.Image getAvatarImage() {
		return this.avatarImage;
	}
	public void setAvatarImage(vn.toancauxanh.gg.model.Image avatarImage1) {
		this.avatarImage = avatarImage1;
	}

	
	public boolean isSelectedDV() {
		return selectedDV;
	}

	public void setSelectedDV(boolean selectedDV) {
		this.selectedDV = selectedDV;
	}

	@Transient
	public AbstractValidator getValidator() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				if (getVaiTros().size() == 0) {
					addInvalidMessage(ctx, "lblErrVaiTros", "Bạn phải chọn vai trò cho người dùng");
				}
			}
		};
	}
	
	@Transient
	public AbstractValidator getValidator(boolean isBackend) {
		return new AbstractValidator() {
			@Override
			public void validate(final  ValidationContext ctx) {
				if (isBackend && getVaiTros() != null && getVaiTros().size() == 0) {
					addInvalidMessage(ctx, "lblErrVaiTros", "Bạn phải chọn vai trò cho người dùng.");
				}
			}
		};
	}
	
	@Transient
	public AbstractValidator getValidatePassword() {
		return new AbstractValidator() {
			@Override
			public void validate(final  ValidationContext ctx) {
				final Object mKhau = ctx.getValidatorArg("password");
				if (mKhau == null) {
				} else {
					Object pass = ctx.getProperty().getValue();
					if (pass == null) {
						pass = "";
					}
					if (mKhau.equals(pass)) {
					} else {
						addInvalidMessage(ctx, "Xác nhận mật khẩu không trùng khớp!");
					}
				}
			}
		};
	}

	@Command
	public void khoaThanhVien(@BindingParam("vm") final Object vm) {
		if ("admin".equals(getMaSinhVien())) {
			showNotification("Không thể khóa SuperUser", "", "warning");
		} else {
			Messagebox.show("Bạn muốn khóa nhân viên này?", "Xác nhận", Messagebox.CANCEL | Messagebox.OK,
					Messagebox.QUESTION, new EventListener<Event>() {
						@Override
						public void onEvent(final Event event) {
							if (Messagebox.ON_OK.equals(event.getName())) {
								setCheckApDung(false);
								save();
								BindUtils.postNotifyChange(null, null, vm, "targetQuerySinhVien");
							}
						}
					});

		}
	}

	@Command
	public void moKhoaThanhVien(@BindingParam("vm") final Object vm) {
		Messagebox.show("Bạn muốn mở khóa sinh viên này?", "Xác nhận", Messagebox.CANCEL | Messagebox.OK,
				Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(final Event event) {
						if (Messagebox.ON_OK.equals(event.getName())) {
							setCheckApDung(true);
							save();
							BindUtils.postNotifyChange(null, null, vm, "targetQuerySinhVien");
						}
					}
				});
	}

	private boolean checkKichHoat;

	public boolean isCheckKichHoat() {
		return checkKichHoat;
	}

	public void setCheckKichHoat(boolean checkKichHoat) {
		this.checkKichHoat = checkKichHoat;
	}

	@Command
	public void toggleLock(@BindingParam("list") final Object obj) {
		String dialogText = "";
		if (!checkKichHoat) {
			dialogText = "Bạn muốn ngưng kích hoạt người dùng đã chọn?";
		} else {
			dialogText = "Bạn muốn kích hoạt người dùng đã chọn?";
		}
		Messagebox.show(dialogText, "Xác nhận", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				new EventListener<Event>() {
					@Override
					public void onEvent(final Event event) {
						if (Messagebox.ON_OK.equals(event.getName())) {
							if (checkKichHoat) {
								setCheckKichHoat(false);
							} else {
								setCheckKichHoat(true);
							}
							save();
							BindUtils.postNotifyChange(null, null, obj, "targetQuerySinhVien");
						}
					}
				});
	}

	@Command
	public void deleteNhanVienInListVaiTro(@BindingParam("vaitro") final VaiTro vt,
			@BindingParam("sinhvien") final SinhVien nv) {
		Messagebox.show("Bạn có chắc chắn muốn xóa vai trò " + vt.getTenVaiTro() + " của nhân viên " + nv.getHoVaTen(),
				"Xác nhận", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(final Event event) {
						if (Messagebox.ON_OK.equals(event.getName())) {
							vaiTros.remove(vt);
							save();
							BindUtils.postNotifyChange(null, null, vt, "listSinhVien");
						}
					}
				});
	}
	
	/*@Command
	public void saveSinhVien(@BindingParam("list") final Object listObject,
			@BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) {
		if (matKhau2 != null && !matKhau2.isEmpty()) {
			updatePassword(matKhau2);
		}
		if (getDonViCon() != null) {
			setDonVi(getDonViCon());
		} else {
			setDonVi(getDonViCha());
		}
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, this, "*");
		BindUtils.postNotifyChange(null, null, listObject, attr);
	}*/
	
	@Command
	public void saveSinhVien(@BindingParam("list") final Object listObject,
			@BindingParam("attr") final String attr) {
		transactioner().execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus arg0) {
				LOG.info("before save artice");
				try {
					//beforeSaveArticle();
				LOG.info("save artice");
				vn.toancauxanh.gg.model.Image avatarImage2 = getAvatarImage();
				if (avatarImage2 != null) {
					if (avatarImage2.getImageContent() == null) {
						avatarImage2.setTrangThai(Labels.getLabel("da_xoa"));
						avatarImage2.saveNotShowNotification();
					} else {
						avatarImage2.setArticlesImage(true);
						avatarImage2.saveNotShowNotification();
					}
				}

				for (FileEntry fileEntry : getFileEntries()) {
					fileEntry.saveNotShowNotification();
				}

				save();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
		BindUtils.postNotifyChange(null, null, listObject, attr);
	}
	
	
	public String getCookieToken(long expire) {
		String token = getId() + ":" + expire + ":";
		return Base64.encodeBase64String(token.concat(DigestUtils.md5Hex(token + matKhau + ":" + salkey)).getBytes());
	}
	
	public void updatePassword(String pass) {
		BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
		String salkey = getSalkey();
		if (salkey == null || salkey.equals("")) {
			salkey = encryptor.encryptPassword((new Date()).toString());
		}
		String passNoHash = pass + salkey;
		String passHash = encryptor.encryptPassword(passNoHash);
		setSalkey(salkey);
		setMatKhau(passHash);
	}
	
	@Transient
	public List<Object> getListThongBao() {
		List<Object> list = new ArrayList<Object>();
		return list;
	}
	
	@Transient
	public AbstractValidator getValidatorEmail() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				String value = (String)ctx.getProperty().getValue();
				if(value == null || "".equals(value)) {
					addInvalidMessage(ctx, "error","Không được để trống trường này");
				}
				else if(!value.trim().matches(".+@.+\\.[a-z]+"))
				{
					addInvalidMessage(ctx, "error","Email không đúng định dạng");
				}
				else 
				{
					JPAQuery<SinhVien> q = find(SinhVien.class)
							.where(QSinhVien.sinhVien.email.eq(value))
							.where(QSinhVien.sinhVien.trangThai.ne(core().TT_DA_XOA));
					if(!SinhVien.this.noId()) {
						q.where(QSinhVien.sinhVien.id.ne(getId()));
					}
					if(q.fetchCount() > 0) {
						addInvalidMessage(ctx, "error","Email đã được sử dụng");
					}
				}
			}
		};
	}
	
	public boolean change = false;
	public boolean editable = false;
	
	@Transient
	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}
	
	@Transient
	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "article_has_file_entry", joinColumns = { @JoinColumn(name = "article_id") }, inverseJoinColumns = { @JoinColumn(name = "file_entry_id") })
	@Fetch(value = FetchMode.SUBSELECT)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	public List<FileEntry> getFileEntries() {
		return this.fileEntries;
	}

	public void setFileEntries(List<FileEntry> fileEntries1) {
		this.fileEntries = fileEntries1;
	}

	@Command
	public void ChangePassword(){
		setChange(isChange() ? false : true);
		BindUtils.postNotifyChange(null, null, this, "change");
	}
	@Command
	public void saveTaiKhoan(){
		if (matKhau2 != null && !matKhau2.isEmpty()) {
			updatePassword(matKhau2);
		}
		save();
		setChange(false);
		setEditable(false);
		BindUtils.postNotifyChange(null, null, this, "change");
		BindUtils.postNotifyChange(null, null, this, "editable");
	}
	@Command 
	public void editableStatus(){
		setEditable(true);
		setChange(true);
		BindUtils.postNotifyChange(null, null, this, "editable");
		BindUtils.postNotifyChange(null, null, this, "change");
	}
}
