package vn.toancauxanh.gg.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.Window;

import vn.toancauxanh.model.Model;

@Entity
@Table(name = "lophoc")
public class LopHoc extends Model<LopHoc>{

	public static transient final Logger LOG = LogManager.getLogger(Khoa.class.getName());
	
	private String tenLop;
	private String description;
	private Khoa khoa;
	
	public LopHoc() {
		super();
	}

	public LopHoc(String tenLop, String description, Khoa khoa) {
		super();
		this.tenLop = tenLop;
		this.description = description;
		this.khoa = khoa;
	}

	public String getTenLop() {
		return tenLop;
	}

	public void setTenLop(String tenLop) {
		this.tenLop = tenLop;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne
	public Khoa getKhoa() {
		return khoa;
	}

	public void setKhoa(Khoa khoa) {
		this.khoa = khoa;
	}
	
	@Command
	public void saveLopHocMain(@BindingParam("list") final Object listObject,
			@BindingParam("wdn") final Window wdn) {
		setTenLop(getTenLop().trim().replaceAll("\\s+", " "));
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, "*");
	}
	
	@Transient
	public AbstractValidator getValidatorLopHoc() {
		return new AbstractValidator() {
			@Override
			public void validate(final ValidationContext ctx) {
				String value = (String)ctx.getProperty().getValue();
				if(value == null || "".equals(value)) {
					addInvalidMessage(ctx, "error","Không được để trống trường này");
				} 
			}
		};
	}
}
