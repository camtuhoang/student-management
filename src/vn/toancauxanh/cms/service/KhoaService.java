package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.Khoa;
import vn.toancauxanh.gg.model.LopHoc;
import vn.toancauxanh.gg.model.QKhoa;
import vn.toancauxanh.gg.model.QLopHoc;
import vn.toancauxanh.service.BasicService;

public class KhoaService  extends BasicService<Khoa>{

	private Khoa selectedKhoa;
	private List<LopHoc> listLop;
	
	public List<LopHoc> getListLop() {
		return listLop;
	}

	public void setListLop(List<LopHoc> listLop) {
		this.listLop = listLop;
	}

	public Khoa getSelectedKhoa() {
		System.out.println("--------------2222------------");
		return selectedKhoa;
	}
	
	public void setSelectedKhoa(Khoa selectedKhoa) {
		//System.out.println("--------------1111------------");
		//System.out.println("selectedKKhoa1: " + selectedKhoa);
		this.selectedKhoa = selectedKhoa;
		//this.listLop = this.getListlopHoc();
	}
	
//	public List<LopHoc> getListlopHoc() {
//		List<LopHoc> list = new ArrayList<LopHoc>();
//		if (selectedKhoa != null) {
//			list = find(LopHoc.class)
//					.where(QLopHoc.lopHoc.trangThai.eq(core().TT_AP_DUNG))
//					.where(QLopHoc.lopHoc.khoa.eq(selectedKhoa))
//					.orderBy(QLopHoc.lopHoc.tenLop.asc())
//					.fetch();
//		}
//		return list;
//	}
	
	public JPAQuery<Khoa> getTargetQuery() {
		String paramImage = MapUtils.getString(argDeco(),Labels.getLabel("param.tukhoa"), "").trim();
		String trangThai = MapUtils.getString(argDeco(),Labels.getLabel("param.trangthai"),"");
		JPAQuery<Khoa> q = find(Khoa.class)
				.where(QKhoa.khoa.trangThai.ne(core().TT_DA_XOA));
		
		if (paramImage != null && !paramImage.isEmpty()) {
			String tukhoa = "%" + paramImage + "%";
			q.where(QKhoa.khoa.description.like(tukhoa));
		}
		if (!trangThai.isEmpty()) {
			q.where(QKhoa.khoa.trangThai.eq(trangThai));
		}
		q.orderBy(QKhoa.khoa.ngaySua.desc());
		return q;
	}
	
	public List<Khoa> getTacGias() {
		// TODO add them dk nhan vien là tác giả
		return getTargetQuery().fetch();
	}
	
	public List<Khoa> getListAllKhoa() {
		JPAQuery<Khoa> q = find(Khoa.class);
		q.where(QKhoa.khoa.trangThai.ne(core().TT_DA_XOA));
				//.where(QLopHoc.lopHoc.parent.isNull());
		//q.orderBy(QLopHoc.lopHoc.soThuTu.asc());
		List<Khoa> list = q.fetch();
		/*for (LopHoc category : list) {
			category.loadChildren();
		}*/
		return list;
	}
	
}
